#!/sbin/sh
# Written by Tkkg1994

getprop ro.boot.bootloader >> /tmp/BLmodel

if grep -q G93 /tmp/BLmodel; then
	mount /dev/block/platform/155a0000.ufs/by-name/USERDATA /data
else
	mount /dev/block/platform/11120000.ufs/by-name/USERDATA /data
fi

if [ -e /data/media/0/IronMan/aroma/restore.prop ]; then
	echo "restore=yes" > /tmp/aroma/restore.prop
fi

exit 10

