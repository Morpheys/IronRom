#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/11120000.ufs/by-name/RECOVERY:38393856:ee984b23e164a78d1366524cd0eef1a216f80ecb; then
  applypatch EMMC:/dev/block/platform/11120000.ufs/by-name/BOOT:33839104:8721c3cc80e252499346dc8254a4d670948a3e29 EMMC:/dev/block/platform/11120000.ufs/by-name/RECOVERY ee984b23e164a78d1366524cd0eef1a216f80ecb 38393856 8721c3cc80e252499346dc8254a4d670948a3e29:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
